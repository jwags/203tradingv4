import { Component } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-navigation-for-home',
  templateUrl: './navigation-for-home.component.html',
  styleUrls: ['./navigation-for-home.component.sass']
})
export class NavigationForHomeComponent {
  constructor() { }
}

$(document).ready(() => {
  // set an offset when navigating to a section via navbar
  $('.scrolling-link').on('click', (event) => {
    event.preventDefault();
    $('#' + $(event.target).attr('navigate-to-id'))[0].scrollIntoView()
    scrollBy(0, +$(event.target).attr('offset'));
  });

  // collapse hamburger menu after navigating (for smaller screens)
  $('.navbar-nav>li>a').on('click', () => {
    $('.navbar-collapse').collapse('hide');
  });
});
