import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationForOtherComponent } from './navigation-for-other.component';

describe('NavigationForOtherComponent', () => {
  let component: NavigationForOtherComponent;
  let fixture: ComponentFixture<NavigationForOtherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationForOtherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationForOtherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
