import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

export class Content {
    title: string;
    body: string;
    icon: IconDefinition;
    isHidden: boolean;
}