import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationForHomeComponent } from './navigation-for-home.component';

describe('NavigationComponent', () => {
  let component: NavigationForHomeComponent;
  let fixture: ComponentFixture<NavigationForHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationForHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationForHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
