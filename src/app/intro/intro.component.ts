import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.sass']
})
export class IntroComponent implements OnInit {
  slideShowList = [
    {
      imgUrl: `/assets/slideshow/slide1.jpg`,
      imgAltText: `203 trading. microsoft. microsoft authorized refurbisher.`,
    },
    {
      imgUrl: `/assets/slideshow/slide2.jpg`,
      imgAltText: `aerial view of warehouse`,
    },
    {
      imgUrl: `/assets/slideshow/slide3.jpg`,
      imgAltText: `inside view of warehouse`,
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}

$(document).ready(() => {
  $('#slideshow-intro').on('slide.bs.carousel', () => {
    setTimeout(() => {
      $('.carousel-item-next, .carousel-item-prev').addClass('d-flex flex-column justify-content-center align-items-center');
    }, 50);
  });
  $('#slideshow-intro').on('slid.bs.carousel', () => {
    $('.carousel-item').removeClass('d-flex flex-column justify-content-center align-items-center');
    $('.carousel-item.active').addClass('d-flex flex-column justify-content-center align-items-center');
  });
});