import { Component, OnInit } from "@angular/core";
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { Content } from '../shared/models/content.model';
declare var $: any;

@Component({
  selector: "app-help",
  templateUrl: "./help.component.html",
  styleUrls: ["./help.component.sass"]
})
export class HelpComponent implements OnInit {
  faPlus = faPlus;
  faMinus = faMinus;
  faqContentList: Content[] = [
    {
      title: `How to Activate Windows`,
      body: `
      <ol>
        <li>Locate license Sticker on bottom of computer</li>
        <li>Copy down license key onto a sticky note</li>
        <li>Start your computer</li>
        <li>When the device boots, a popup will display asking for the license key</li>
        <li>Enter the key from the sticky note and follow the on-screen instructions</li>
      </ol>`
    } as Content,
    {
      title: `How to Wipe Hard Drive`,
      body: `
      <ol>
        <li>Turn off the device</li>
        <li>Turn the device on and quickly press f12 repeatedly</li>
        <li>Do this until a boot menu displays</li>
        <li>Select "Whipe hard drive"</li>
        <li>Follow on-screen instructions</li>
      </ol>`
    } as Content,
    {
      title: `How to Replace Battery`,
      body: `
      <ol>
        <li>Turn off your laptop</li>
        <li>Unplug the device</li>
        <li>Flip the device over</li>
        <li>Slide the switch to the right</li>
        <li>Pull the battery twords you</li>
        <li>Take new batter and push it into the slot until it clicks</li>
      </ol>`
    } as Content
  ] as Content[];

  constructor() {}

  ngOnInit() {}

  toggleContent(event): void {
    // Prevent animation if user is selecting text (like to copy to clipboard)
    var anySelectedText = !!getSelection().toString();
    if (!anySelectedText) {
      event.preventDefault();

      // Get context
      const _this = event.target || event.srcElement || event.currentTarget;
      const containerForFaqContent = $(_this).closest(".faq-content");

      // Prevent selecting text while animation is in progress
      $(containerForFaqContent).addClass("noselect");

      // Hide content
      // Stop() is used to cancel animation if user rapidly clicks faq-content box
      $(".body", containerForFaqContent)
        .stop()
        .slideToggle(() => {
          // Toggle which icon to display (plus vs minus)
          // It is nested because when it was not and the
          // user clicked very quickly, they would become
          // out of sync. This is my fix.
          if (!!$(".body", containerForFaqContent).is(":visible")) {
            $("fa-icon.plus-icon", containerForFaqContent).addClass("d-none");
            $("fa-icon.minus-icon", containerForFaqContent).removeClass(
              "d-none"
            );
          } else {
            $("fa-icon.plus-icon", containerForFaqContent).removeClass(
              "d-none"
            );
            $("fa-icon.minus-icon", containerForFaqContent).addClass("d-none");
          }

          // Allow selecting text after animation completes
          $(containerForFaqContent).removeClass("noselect");
        });
    }
  }
}
