import { Component, OnInit } from "@angular/core";
import { Content } from "../shared/models/content.model";
import {
  faBoxOpen,
  faPlaneDeparture,
  faTools,
  faLaptop,
  faRecycle,
  faClipboardCheck,
  faShieldAlt,
  faComment
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-services",
  templateUrl: "./services.component.html",
  styleUrls: ["./services.component.sass"]
})
export class ServicesComponent implements OnInit {
  servicesList: Content[] = [
    {
      title: `Wholesale Oppurtunities`,
      body: `We sell to customers big and small. If you are looking for a personal computer or to fulfill the needs of an entire department or company, we are the best choice. We fulfill both small and large orders and we do so quickly so you have the equipment you need.`,
      icon: faBoxOpen,
      isHidden: true
    } as Content,
    {
      title: `Domestic and International Clients`,
      body: `We welcome customers located all over the world. We have experience shipping across the state, across the country, or overseas. We will make sure your product arrives at your doorstep with affordable shipping rates.`,
      icon: faPlaneDeparture,
      isHidden: true
    } as Content,
    {
      title: `Refurbishment Services`,
      body: `We are a trusted Microsoft Authorized Refurbisher because we specialize in refurbished computers. That way you can get a product that works and looks like new but for a fraction of the cost.`,
      icon: faLaptop,
      isHidden: true
    } as Content,
    {
      title: `Expert Repair`,
      body: `Our computer techs are trained thoroughly to make sure your purchase arrives in pristine and working conditions. They utilize various tools to identify potential issues, test hardware, and install necessary software so your computer runs smoothly.`,
      icon: faTools,
      isHidden: true
    } as Content,
    {
      title: `Reduce Reuse Recycle`,
      body: `We believe we can make the world a better place by simply reducing our carbon footprint. We do this by reusing every part we can, repairing what we can, and when this is not possible, recycling what is left over.`,
      icon: faRecycle,
      isHidden: true
    } as Content,
    {
      title: `Quality Assurance`,
      body: `Nothing is worse than waiting eagerly for a product you purchased to arrive, opening the packaging just to discover it is broken. That is why we triple check every computer is in grade-A condition before it is shipped in our secure packaging so it arrives in pristine condition, ready to use.`,
      icon: faClipboardCheck,
      isHidden: true
    } as Content,
    {
      title: `Secure Data Management`,
      body: `Data management is no joke. Sensitive data gets leaked too often and so we take this very seriously. We use state-of-the-art technology to protect the data of our customers to make sure the information on every computer we ship is completely removed so it cannot be recovered.`,
      icon: faShieldAlt,
      isHidden: true
    } as Content,
    {
      title: `Customer Satisfaction`,
      body: `Our in-house customer service team is dedicated to making sure you are happy with your purchase. They are trained on every product we sell so they can answer any questions you have. We rely on our customers so their satisfaction is our top priority. `,
      icon: faComment,
      isHidden: true
    } as Content
  ] as Content[];

  constructor() {}

  ngOnInit() {}
}
