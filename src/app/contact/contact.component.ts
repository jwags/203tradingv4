import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent {
	contactUsForm: FormGroup;
  submitted: boolean = false;
  messageSent: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService) {
		this.contactUsForm = this.formBuilder.group({
			name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
			email: ['', Validators.compose([Validators.required, Validators.email, Validators.maxLength(50)])],
			message: ['', Validators.compose([Validators.required, Validators.maxLength(5000)])]
		});
  }

  submitContactUsForm(): void {
    this.submitted = true;
    if (this.contactUsForm.invalid) {
      return;
    }

    this.toastr.success('Your message has been sent. We will respond within the next 48 business hours.', 'Message Sent', {
      timeOut: 3000,
      extendedTimeOut: 3000,
      progressAnimation: 'decreasing',
      progressBar: true,
      closeButton: true,
      positionClass: 'toast-bottom-right'
    });

    this.messageSent = true;
  }
}
